#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Server monitoring system
#
# Copyright © 2011 Rodrigo Eduardo Lazo Paz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


"""Launching script."""


__author__ = "rlazo.paz@gmail.com (Rodrigo Lazo)"
__version__ = 0.2


import argparse
import time
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "lib"))

from multiprocessing import Process, managers
from monitor.collector import HttpCollector
from monitor import http_display
from monitor import datastore


MANAGER_PORT = 0                # auto-generated
MANAGER_AUTHKEY = 'xlz98'


class MonitorManager(managers.SyncManager):
    """Custom multiprocessing manager."""
    pass


def timed_collector(sources, interval):
    """Runs collector in intervals.

    This method executes and infinite loop collecting data each
    `interval` seconds. The `DataStore` object used to store the
    retrieved data is a remote object obtained from a `MonitorManager`.


    Arguments:
    - `sources`: Iterable, pairs of (hostname, port) to use for collection.
    - `interval`: Integer, seconds between collections.

    """
    manager = MonitorManager(address=('localhost', MANAGER_PORT),
                             authkey=MANAGER_AUTHKEY)
    manager.connect()
    store = manager.get_store()
    collector = HttpCollector(sources)
    while (True):
        collected_data = collector.collect()
        store.insert_dict(collected_data)
        time.sleep(interval)


def displayer(port):
    """Runs the http exporter interface.

    This method executes an HTTP server
    (`monitor.displayer.http_display`). The `DataStore` object used to
    call this method is a remote object obtained from a
    `MonitorManager`.

    """
    manager = MonitorManager(address=('localhost', MANAGER_PORT),
                             authkey=MANAGER_AUTHKEY)
    manager.connect()
    store = manager.get_store()
    http_display.main(store, port,
                      "/home/rodrigo/workspace/monitor/monitor/www_templates/")


def main():
    """Main function, launches process for each part of the system."""
    parser = argparse.ArgumentParser(description="Server monitoring system.")
    parser.add_argument('--http-port', metavar="PORT", type=int, default=9012,
                        help="Port for the HTTP server (displayer).")
    parser.add_argument('--collection-interval', metavar="SECONDS",
                        type=int, default=30,
                        help="Interval, in seconds, between data collections.")

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--remote-servers', nargs="+",
                       help=("List, separated by spaces, of remote servers "
                             "(HOST:PORT) to monitor."))
    group.add_argument('--snapshot', metavar="SNAPSHOT-FILE", type=str,
                       help="Filename of the snapshot to load.")
    args = parser.parse_args()

    if args.snapshot:
        mstore = manager.get_store()
        mstore.load(args.snapshot)
    else:
        sources = [x.split(':') for x in args.remote_servers]
        collector_p = Process(target=timed_collector,
                              args=(sources, args.collection_interval))
        collector_p.start()
    displayer_p = Process(target=displayer, args=(args.http_port,))
    displayer_p.start()
    if not args.snapshot:
        collector_p.join()
    displayer_p.join()


if __name__ == '__main__':
    store = datastore.DataStore()
    MonitorManager.register('get_store', callable=lambda: store)
    manager = MonitorManager(address=('', 0),
                             authkey=MANAGER_AUTHKEY)
    manager.start()
    MANAGER_PORT = manager.address[-1]
    main()
