#! /usr/bin/env python
#
# -*- coding: utf-8 -*-

"""Comment
"""

__author__ = "rlazo.paz@gmail.com (Rodrigo Lazo)"

import unittest
from datastore import DataEntry, DataPoint
from operator import attrgetter, itemgetter


class TestDataEntry(unittest.TestCase):
    def setUp(self):
        self.entry = DataEntry()

    def test_invalid_single_inserts(self):
        self.assertFalse(self.entry.insert_point(-1209, 2),
                         "Negative timestamp accepted.")
        self.assertFalse(self.entry.insert_point(12.1, 34830923),
                         "Float timestamp accepted.")
        self.assertFalse(self.entry.insert_point("23", 34830923),
                         "Str timestamp accepted.")
        self.assertEqual(len(self.entry.get_all()), 0,
                         "Only invalid inserts tried, it must be empty.")

    def test_invalid_batch_inserts(self):
        self.assertFalse(self.entry.insert_points([(-1209, 2)]),
                         "Negative timestamp accepted.")
        self.assertEqual(len(self.entry.get_all()), 0,
                         "Only invalid inserts tried, it must be empty.")
        data_points = ((232, 2), (-193, 3), ("st", 5), (182, 4), (2.1, 4))
        self.assertFalse(self.entry.insert_points(data_points),
                         "Invalid timestamps accepted.")
        self.assertEqual(len(self.entry.get_all()), 2,
                         "Only two valid entries submitted.")

    def test_valid_single_insert(self):
        self.assertTrue(self.entry.insert_point(23, 393),
                        "Valid value rejected.")
        self.assertTrue(self.entry.insert_point(0, "no problemo"),
                        "Valid value rejected.")
        self.assertEqual(len(self.entry.get_all()), 2,
                         "Lenght doesn't match, only 2 insertions")

    def test_valid_batch_inserts(self):
        values = ((19282, "x"), (00, 338), (5, 0.53))
        self.assertTrue(self.entry.insert_points(values),
                        "Valid values rejected.")
        self.assertEqual(len(self.entry.get_all()), len(values),
                         "Lenght doesn't match, 5 insertions")

    def test_get_latest(self):
        values = ((2, "x"), (48281, "m"), (100000000, 2), (1, "sor"))
        latest = max(values, key=itemgetter(0))
        self.entry.insert_points(values)
        self.assertEqual(self.entry.get_latest().timestamp, latest[0],
                         "latest value must be %s:%s" % latest)

    def test_points_sorted(self):
        values = ((2, "x"), (48281, "m"), (100000000, 2), (1, "sor"))
        self.entry.insert_points(values)
        self.assertTrue(is_sorted(self.entry.get_all(),
                                  func=attrgetter("timestamp")),
                        "Points are not sorted by timestamp.")

    def test_get_since(self):
        values = ((10, None), (30, None), (50, None), (60, None), (70, None))
        points = [DataPoint(*i) for i in values]

        self.entry.insert_points(values)
        result = self.entry.get_since(100000)
        self.assertFalse(result, "Timestamp in the future should return empty,"\
                         " got list of size %d" % len(result))
        result = self.entry.get_since(70.1)
        self.assertFalse(result, "Timestamp in the future should return empty,"\
                         " got list of size %d" % len(result))

        result = self.entry.get_since(0)
        self.assertEquals(result, points,
                        "Timestamp 0 should return all points,"\
                         " got %s" % str(result))

        self.assertEquals(self.entry.get_since(10), points)
        self.assertEquals(self.entry.get_since(11), points[1:])
        self.assertEquals(self.entry.get_since(29), points[1:])
        self.assertEquals(self.entry.get_since(30), points[1:])
        self.assertEquals(self.entry.get_since(40), points[2:])
        self.assertEquals(self.entry.get_since(70), points[4:])


def is_sorted(container, func=None):
    if func:
        return all(func(container[i]) <= func(container[i+1])
                   for i in xrange(len(container)-1))
    return all(container[i] <= container[i+1]
               for i in xrange(len(container)-1))


if __name__ == '__main__':
    unittest.main()
