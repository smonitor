#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Server monitoring system
#
# Copyright © 2011 Rodrigo Eduardo Lazo Paz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


"""Multi-dimensional Store.

Provides the DataStore class, a sparse three-dimensional storage data
structure. It stores data points, or vars, identified by name. Each
var has multiple timestamped (unix time) values, or `DataPoint`s,
grouped together into a `DataEntry`, sorted in descending order,
newest value first. For example, var `stock_quote` could have the
values (1316516400, 21.1), (1316512800, 21.3), (1316509200, 20.3)
representing the stock values during three consecutive hours on
09/20/11.

Vars belong to a group. Each group can have any number of vars. Using
the same var name in multiple groups simulates the concept of column
in SQL databases.

Data is persistent through Python's Pickle interface, therefore, the
three classes in this module, `DataPoint`, `DataEntry` and
`DataStore`, must be imported when using the dump/load methods,
otherwise, Python may complain about missing class declarations.
"""

__author__ = "rlazo.paz@gmail.com (Rodrigo Lazo)"
__version__ = 0.2


import bisect
from itertools import groupby
import cPickle
import collections
import os


class DataPoint(object):        # pylint: disable=R0903
    """Single data point.

    Stores a value, any type, and the timestamp associated with
    it. Provides comparison based solely on the timestamp. Any
    comparison made to non `DataPoint` objects is always -1.
    """
    def __init__(self, timestamp, value):
        self.timestamp = timestamp
        self.value = value

    def __cmp__(self, other):
        if not isinstance(other, DataPoint):
            return -1
        return cmp(self.timestamp, other.timestamp)

    def __str__(self):
        return "%s@%d" % (str(self.value), self.timestamp)

    def as_tuple(self):
        """Returns a tuple (timestmap, value)."""
        return (self.timestamp, self.value)


# TODO: (09/20) compare performance of insert_point and insert_point
#       using biesct.insort and sort
class DataEntry(object):
    """List of related `DataPoint`s sorted by newest first."""

    def __init__(self):
        self._points = []

    def insert_point(self, timestamp, value):
        """Inserts a single data point.

        Arguments:
        - `timestamp`: Positive integer, unix time associated with value.
        - `value`: Data to insert.

        Returns:
          True if inserted, False otherwise. Only an invalid timestamp
          will cause the operation to return False.
        """
        if not isinstance(timestamp, int) or timestamp < 0:
            return False
        bisect.insort(self._points, DataPoint(timestamp, value))
        return True

    def insert_points(self, values):
        """Inserts a list of values.

        Arguments:
        - `values`: Iterable, containing pairs of (timestamp,
          value). `timestamp` must be positive integer, `value` can be
          any object.

        Returns:
          True if all the points in `values` could be correctly
          inserted, False otherwise. Only an invalid timestamp will
          cause the operation to return False.
        """
        flag = True
        for timestamp, value in values:
            if not isinstance(timestamp, int) or timestamp < 0:
                flag = False
            else:
                self._points.append(DataPoint(timestamp, value))
        self._points.sort()
        return flag

    def get_latest(self):
        """Returns the latest, by timestamp, `DataPoint`."""
        return self._points[-1]

    def get_all(self):
        """Returns an interable of all `DataPoints`, sorted by timestmap."""
        return self._points

    def get_all_compact(self):
        """Returns an iterable of distinct `DataPoints`, sorted by timestamp.

        If consecutives `DataPoints` have the same value, only two are
        returned, the first and last by timestamp.
        """
        result = []
        for _, iterable in groupby(self._points, key=lambda x: x.value):
            values = list(iterable)
            result.append(values[0])
            if len(values) >= 2:
                result.append(values[-1])
        return result

    def get_max_value(self):
        """Returns the largest value stored."""
        return max(self._points, key=lambda x: x.value).value

    def get_min_value(self):
        """Returns the largest value stored."""
        return min(self._points, key=lambda x: x.value).value

    def get_since(self, timestamp):
        """Builds an iterable of `DataPoints` since `timestamp`.

        Arguments:
        - `timestamp`: Positive integer, represents the timestamp of
          the earliest `DataPoint` to return.

        Returns:
          An iterable of sorted, by timestamp, `DataPoints` whose
          timestamp value is greater or equal to `timestamp` argument.
        """
        dummy_point = DataPoint(timestamp, None)
        index = bisect.bisect(self._points, dummy_point)
        if index > 0 and self._points[index - 1] == dummy_point:
            index -= 1
        return self._points[index:]


# TODO: (09/20) Make the object thread-safe.
class DataStore(object):
    """Multi-dimensional data store.

    See file level comments for further information.
    """

    def __init__(self):
        self._store = collections.defaultdict(dict)
        self._vars = set()
        self._is_snapshot = False

    def insert(self, group, var, timestamp, value):
        """Inserts a single data point.

        Arguments:
        - `group`: String, point's group name
        - `var`: String, point's var name
        - `timestamp`: Positive integer, timestamp associated with
          this point
        - `value`: Object, data to store.

        Returns:
          True, if value was correctly inserted, False otherwise. Only
          invalid timestamp values will cause the rejection of an insert.
        """
        return self._store[group].setdefault(
            var, DataEntry()).insert_point(timestamp, value)

    def insert_dict(self, data_dict):
        """Inserts multiple data points.

        `data_dict` must be a dictionary of values in the form:

        {'group': {'var': (timestamp, val), "var": (timestamp, val)}}

        See `DataStore.insert` for detailed definition of Valid values
        for each element of the dictionary.

        Returns:
          True, if all values were correctly inserted, False
          otherwise. Only invalid timestamp values will cause the
          rejection of an insert.
        """
        result = True
        for group_name, entry in data_dict.iteritems():
            for var_name, datapoint in entry.iteritems():
                result = result and \
                         self.insert(group_name, var_name,
                                     datapoint[0], datapoint[1])
        return result

    def get_group(self, group):
        """Lists all vars, and corresponding `DataEntries', for `group`.

        Returns:
          A dictionary in the form {'varname': `DataEntry`}, or
          empty if group does not exist or doesn't contains data.
        """
        return self._store[group] if group in self._store else {}

    def get_var(self, var):
        """Lists all groups, and corresponding `DataEntries`, for `var`.

        Returns:
          A dictionary in the form {'hostname': `DataEntry`}, or
          empty if var does not exist.
        """
        self._update_vars()
        sol = {}
        if var in self._vars:
            for group in self._store.iterkeys():
                if var in self._store[group]:
                    sol[group] = self._store[group][var]
        return sol

    def list_groups(self):
        """Returns a list contaning the name of every group store."""
        return self._store.keys()

    def list_vars(self):
        """Returns a list contaning the name of every var store."""
        self._update_vars()
        return list(self._vars)

    def load(self, filename):
        """Loads data from `filename`.

        Any internal data stored will be deleted before loading the
        file.

        Arguments:
        - `filename`: String, path to a file created by
          `DataStore.dump` method.
        """
        with open(filename, 'rb') as fd:
            obj = cPickle.load(fd)
            self._store = obj
            self._is_snapshot = True
            self._vars.clear()

    def dump(self, filename):
        """Creates an snapshot of this object.

        Generated file is binary. For a textual representation of the
        data, see `DataStore.dump_as_text`.

        Arguments:
        - `filename`: String, path of the file to create/overwrite.
        """
        with open(filename, 'wb', 0) as fd:
            cPickle.dump(self._store, fd, cPickle.HIGHEST_PROTOCOL)
            fd.flush()
            os.fsync(fd.fileno())

    def dump_obj(self):
        """Creates a snapshot of this objects and returns it as an object."""
        return cPickle.dumps(self._store)

    def dump_as_text(self, filename):
        """Creates a human-readable snapshot of this object.

        The file created by this method cannot be loaded again. To
        create an snapshot for data persistency, see `DataStore.dump`.

        Arguments:
        - `filename`: String, path of the file to create/overwrite.
        """
        with open(filename, 'w') as fd:
            for groupname, varss in self._store.iteritems():
                for varname, entry in varss.iteritems():
                    points = (str(x.as_tuple()) for x in entry.get_all())
                    fd.write("%s@%s: %s\n" % (varname, groupname,
                                              " ".join(points)))
            fd.flush()
            os.fsync(fd.fileno())

    def is_snapshot(self):
        return self._is_snapshot

    def _update_vars(self, force=False):
        """Updates internal _vars cache."""
        if not self._vars or force:
            self._vars.update(*[v.keys() for v in self._store.itervalues()])
