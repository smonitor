#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Server monitoring system
#
# Copyright © 2011 Rodrigo Eduardo Lazo Paz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


"""Http/html Exporter module.

Based on CherryPy framework, this module exposes the collected data
through HTTP. Data is formatted in html, which is generated based on a
very simple template system, based on Python's `string.Template`
functionality. Refer to each method of `MonitorHttpHandler` decorated
with `@cherry.expose` for the name of the template file used an the
list of variables available.

The intended entry point for this module is the `main` function, which
instantiates and configures the HTTP server. For further details,
refer to its docstring.
"""

__author__ = "rlazo.paz@gmail.com (Rodrigo Lazo)"
__version__ = 0.2

from datetime import datetime

import cherrypy
from cherrypy.lib.static import serve_fileobj
from jinja2 import Environment, PackageLoader

from datastore import DataStore, DataEntry, DataPoint  # pylint: disable=W0611

def timestampformat(value, format_str='%H:%M:%S'):
    return datetime.fromtimestamp(value).strftime(format_str)

# TODO: (09/19) add support for custom made plots based on GET request
#       params, providing an on-line plotting tool.
# TODO: (09/19) evaluate alternatives to error messages, perhaps 404?
# TODO: (09/19) add more variables to the templates
class MonitorHttpHandler(object):
    """Custom HTTP handler class."""
    def __init__(self, store, templates_path):
        self._store = store
        self._templates_path = templates_path
        self.env = Environment(loader=PackageLoader('monitor', 'www_templates'))
        self.env.filters["timestampformat"] = timestampformat

    @cherrypy.expose
    def index(self):
        """Web system root url ('/').

        Template: index.html
        Vars: title, timestamp, body.
        """
        template = self.env.get_template('index.html')
        references = self._get_default_references()
        references.update({"title": "Monitoring",
                           "body": "<h1>seee</h1>"})
        return template.render(references)

    @cherrypy.expose
    def vars(self):
        """Displays a list of collected vars.

        Each item in the list links to a page (`var?name=varname`)
        which displays more detailed information.

        Template: simple.html
        Vars: title, timestamp, body.
        """
        template = self.env.get_template('vars.html')
        references = self._get_default_references()
        references.update({"title": "List of monitored vars",
                           "vars" : self._store.list_vars()})
        return template.render(references)

    @cherrypy.expose
    def hosts(self):
        """Displays a list of monitored hosts.

        Each item in the list links to a page (`host?name=hostname`)
        which displays more detailed information.

        Template: simple.html
        Vars: title, timestamp, body.
        """
        template = self.env.get_template('hosts.html')
        references = self._get_default_references()
        references.update({"title": "List of monitored vars",
                           "vars" : self._store.list_groups()})
        return template.render(references)

    @cherrypy.expose
    def var(self, name):
        """Displays information about a single variable.

        Shows a consolidated plotting of collected data by hosts.

        Args:
        - `name`: String, variable to display

        Template: simple.html
        Vars: title, timestamp, body.
        """
        template = self.env.get_template('var.html')
        references = self._get_default_references()
        references["title"] = "Historic Values of var: %s" % name
        references["data"] = self._store.get_var(name)
        value_range = set()
        for value in references["data"].itervalues():
            value_range.update([x.value for x in value.get_all_compact()])
            if len(value_range) > 1:
                break
        else:
            references["single_value"] = value_range.pop()

        miny = min([min(a.get_all(), lambda x: x.value)
                    for a in references["data"].values()])
        maxy = max([max(a.get_all(), lambda x: x.value)[1]
                    for a in references["data"].values()])
        print miny, maxy
        return template.render(references)

    @cherrypy.expose
    def host(self, name):
        """Displays information about a single host.

        Shows a series of plots displaying each collected variable
        individually.

        Args:
        - `name`: String, host to display.

        Template: simple.html
        Vars: title, timestamp, body.
        """
        template = self.env.get_template('host.html')
        references = self._get_default_references()
        references["title"] = "Historic Values of host: %s" % name
        references["data"] = self._store.get_group(name).items()
        return template.render(references)

    @cherrypy.expose
    def snapshot(self):
        """Returns a snapshot of the state of the database."""
        filename = "snapshot-%s.pickle" % \
                   datetime.strftime(datetime.now(), "%d%m%y-%H%M")
        return serve_fileobj(self._store.dump_obj(),
                             disposition="attachment",
                             content_type="application/octet-stream",
                             name=filename)

    def _get_default_references(self):
        """Returns a dic with basic references already set.

        Includes:
        - timestamp (current timestamp).
        - is_snapshot (if current data store is a snapshot)
        """
        return {"timestamp": str(datetime.now()),
                "is_snapshot": self._store.is_snapshot()}



def main(store, port, templates_path):
    """HTTP server starter.

    This is a blocking function.

    Arguments:
    - `store`: DataStore, where to extract the data.
    - `port`: Integer, server port.
    - `templates_path`: String, fullpath of the templates directory
    """
    conf = {"global":  {'server.socket_port': port}}

    cherrypy.quickstart(MonitorHttpHandler(store, templates_path),
                        config=conf)
