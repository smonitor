#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Server monitoring system
#
# Copyright © 2011 Rodrigo Eduardo Lazo Paz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


"""Data collection routines.

Collectors are classes which retrieve data from remote
servers. Currently, there is only one class implement which makes
parallel request to http servers.
"""

__author__ = "rlazo.paz@gmail.com (Rodrigo Lazo)"
__version__ = 0.2


import Queue
import contextlib
import threading
import time

from urllib2 import urlopen, URLError


REQUEST_TIMEOUT = 5             # Seconds to timeout remote requests


# TODO: (09/19) add support for string values (Boolean?).
# TODO: (09/19) make parsing resilient to spaces in values.
# TODO: (09/20) logging facilities
def _retriever(source, timestamp, output_queue):
    """Fetches and parses data from a remote source.

    Queries a remote http server using the url

    http://host:port/?format=text

    It expects a plain text (no html formatting) list of values, with
    a single data point per line, in the form:

    [id@]varname: value

    - `id@` is an optional variable name prefix that will be discarted
      during parsing.
    - `varname` is the actual varname and must not contain '@' or ' '.
    - `value` must be a integer(42) or float(3.14159); any space will
      break data parsing.

    Collected data will be output to the `output_queue` as a pair:
    ("ip:port", {"varname": (timestamp, value)}).

    Args:
    - `source`: Pair, (ip, port) address of source http server.
    - `timestamp`: Integer, unix time to use as collected data's timestamp.
    - `output_queue`: Queue, where to output the collected data, in
      the form

    """
    host_address = "%s:%s" % (source[0], source[1])
    url = "http://%s/?format=text" % host_address
    vars_dict = {}
    try:
        with contextlib.closing(urlopen(url, timeout=REQUEST_TIMEOUT)) as fd:
            for line in fd:
                elements = line.split()
                if (len(elements) == 2):
                    fullvarname, value = elements
                    varname = fullvarname.split('@')[-1]
                    if value == 'true':
                        value = 1
                    elif value == 'false':
                        value = 0
                    elif '.' in value:
                        value = float(value)
                    else:
                        value = int(value)
                    vars_dict[varname] = (timestamp, value)
        output_queue.put((host_address, vars_dict))
    except URLError:
        print "ERROR RETRIEVING %s" % host_address
        output_queue.put((None, None))


class HttpCollector(object):
    """Retrieves data from HTTP sources.

    Registers a list of data source server, which must expose data
    using plain-text format through HTTP and collects their data. A
    simple use-case is to create a scheduling mechanism that initiates
    repetitive collections and feed this information into a
    database. For more details about the data formatting, see the
    `HttpCollector.collect` method.
    """

    def __init__(self, sources):
        """Constructor.

        Arguments:
        - `sources`: Iterable, pairs of (ip, port) of data sources.
        """
        self._sources = sources
        self._last_collected = {}
        self._timestamp = 0

    def collect(self):
        """Retrieves data form the sources.

        Multi-threated data retrieval. For details about data
        processing see `_retriever`.

        Returns:
          Dictionary, {"ip:port": {"varname": (timestamp, value)}}.
        """
        result = {}
        queue = Queue.Queue()
        timestamp = int(time.time())
        threads = []
        for source in self._sources:
            threads.append(threading.Thread(target=_retriever,
                                            args=(source, timestamp, queue)))
            threads[-1].start()
        for _ in range(len(self._sources)):
            host_address, data = queue.get()
            if host_address is not None:
                result[host_address] = data
        self._last_collected = result
        self._timestamp = timestamp
        return result

    def get_collected_data(self):
        """Returns a copy of the last collected data.

        See `HttpCollector.collect` for details about the returned data format.
        """
        return self._last_collected

    def get_last_collection_timestamp(self):
        """Timestamp of the lastest collection.

        Returned valu is the unix time (as integer) of the latest
        collection, or 0 no collection was performed.
        """
        return self._timestamp  # 0 if never done
